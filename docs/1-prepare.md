# 动手前必读
## 1、使用公有云服务器的同学看这里
用云服务器的同学们，**跳过keepalived的章节，不要用虚拟ip**（云环境一般都不支持自己定义虚拟ip）就直接把虚拟ip设置为第一个master的ip就可以了（global-config.properties文件中配置【下文第五节 - 准备配置文件】）。  
> PS：如果是一定要高可用的话可以购买云商的负载均衡服务（比如阿里云的SLB），把backends设置成你的三个master节点，然后虚拟ip就配置成负载均衡的内网ip即可。

# 一、实践环境准备
## 1. 服务器说明
我们这里使用的是五台centos 7.2实体机，具体信息如下表：

| 系统类型 | IP地址 | 节点角色 | CPU | Memory | Hostname |
| :------: | :--------: | :-------: | :-----: | :---------: | :-----: |
| centos-7.2 | 172.18.41.18 | master |   \>=2    | \>=2G | m7-a2-15-41.18-jiagou.cn |
| centos-7.2 | 172.18.41.19 | master |   \>=2    | \>=2G | m7-a2-15-41.19-jiagou.cn |
| centos-7.2 | 172.18.41.20 | master |   \>=2    | \>=2G | m7-a2-15-41.20-jiagou.cn |
| centos-7.2 | 172.18.64.41 | worker |   \>=2    | \>=2G | syq-g05-64.41-jiagou.cn |
| centos-7.2 | 172.18.64.42 | worker |   \>=2    | \>=2G | syq-g05-64.42-jiagou.cn |

## 2. 系统设置（所有节点）
#### 2.1 主机名
主机名必须每个节点都不一样，并且保证所有点之间可以通过hostname互相访问。
```bash
# 查看主机名
$ hostname

# 修改主机名
$ hostnamectl set-hostname <your_hostname>

# 配置host，使主节点之间可以通过hostname互相访问
$ vi /etc/hosts
# <node-ip> <node-hostname>
```
#### 2.2 时间设置
集群中所有的机器时间必须一致。
```bash
# 查看时间 确保所有的节点日期信息一致
$ date
# 如果不一致 安装ntpdate，并进行时间同步
$ yum install -y ntpdate
# 从阿里云同步时间
$ ntpdate ntp1.aliyun.com
# 系统时间同步到硬件，防止系统重启后时间还原
$ hwclock --systohc
```
#### 2.3 安装依赖包
```bash
# 更新yum
$ yum update

# 安装依赖包
$ yum install -y conntrack ipvsadm ipset jq sysstat curl iptables libseccomp
```
#### 2.4 关闭防火墙、swap，重置iptables
```bash
# 关闭防火墙
$ systemctl stop firewalld && systemctl disable firewalld

# 重置iptables
$ iptables -F && iptables -X && iptables -F -t nat && iptables -X -t nat && iptables -P FORWARD ACCEPT

# 关闭swap 在所有的节点上 包括主节点和woker节点
# 切记一定要关闭 不然 kubelet启动失败 血的教训
$ swapoff -a
# 禁止swap开机启动
$ sed -i '/swap/s/^\(.*\)$/#\1/g' /etc/fstab

# 关闭selinux
$ vim /etc/selinux/config 
# 将SELINUX=enforcing改为SELINUX=disabled
$ setenforce 0
$ getenforce ##检查selinux状态

# 关闭dnsmasq(否则可能导致docker容器无法解析域名)
$ systemctl stop dnsmasq && systemctl disable dnsmasq
```
#### 2.5 系统参数设置

```bash
# 制作配置文件
$ cat > /etc/sysctl.d/kubernetes.conf <<EOF
net.bridge.bridge-nf-call-iptables=1
net.bridge.bridge-nf-call-ip6tables=1
net.ipv4.ip_forward=1
vm.swappiness=0
vm.overcommit_memory=1
vm.panic_on_oom=0
fs.inotify.max_user_watches=89100
EOF

# 生效文件
$ sysctl -p /etc/sysctl.d/kubernetes.conf
```
## 3. 安装docker（仅worker节点安装，主节点不需要安装docker）

```bash
# 1. 方法一: 通过yum源的方式安装
# 创建所需目录
$ mkdir -p /opt/kubernetes/docker && cd /opt/kubernetes/docker
# 清理原有版本, 如果系统没有安装过跳过
$ yum remove -y docker* container-selinux
# 安装依赖包
$ yum install -y yum-utils device-mapper-persistent-data lvm2
# 设置yum源
$ yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# 可以查看所有仓库中所有docker版本，并选择特定版本安装：
$ yum list docker-ce --showduplicates | sort -r
# 安装指定版本docker, 如果不指定版本号，将安装最新版本的docker
$ sudo yum install -y docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
# 示例-安装docker版本是: 18.09.2
$ yum install -y docker-ce-18.09.2 docker-ce-cli-18.09.2 containerd.io
# 开机启动
$ systemctl enable docker && systemctl start docker

# 2. 方法二: 通过rpm方式安装
# 手动下载rpm包【示例docker 19.03.8版本的安装】
$ wget https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.2.el7.x86_64.rpm
$ wget https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-19.03.8-3.el7.x86_64.rpm
$ wget https://download.docker.com/linux/centos/7/x86_64/stable/Packages/docker-ce-cli-19.03.8-3.el7.x86_64.rpm
# 安装rpm包
$ yum localinstall -y *.rpm
# 开机启动
$ systemctl enable docker && systemctl start docker


# 设置参数
# 1.查看磁盘挂载
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda2        98G  2.8G   95G   3% /
devtmpfs         63G     0   63G   0% /dev
/dev/sda5      1015G  8.8G 1006G   1% /tol
/dev/sda1       197M  161M   37M  82% /boot
# 2.设置docker启动参数
# - 设置docker数据目录：选择比较大的分区（我这里是根目录就不需要配置了，默认为/var/lib/docker）
# - 设置cgroup driver（默认是cgroupfs，主要目的是与kubelet配置统一，这里也可以不设置后面在kubelet中指定cgroupfs）
$ cat <<EOF > /etc/docker/daemon.json
{
    "graph": "/docker/data/path",
    "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
# 启动docker服务
systemctl start docker

# 设置参数
# 1.查看磁盘挂载
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda2        98G  2.8G   95G   3% /
devtmpfs         63G     0   63G   0% /dev
/dev/sda5      1015G  8.8G 1006G   1% /home
/dev/sda1       197M  161M   37M  82% /boot

# 2.选择比较大的分区（我这里是/home）
$ mkdir -p /home/docker-data
$ cat <<EOF > /etc/docker/daemon.json
{
    "graph": "/home/docker-data"
}
EOF
# daemon.json 详细配置示例
{
  "debug": false,
  "experimental": false,
  "graph": "/home/docker-data",
  "registry-mirrors": [
    "https://fy707np5.mirror.aliyuncs.com"
  ],
  "insecure-registries": [
    "hub.zy.com"
  ]
}

# 启动docker服务
systemctl restart docker
```

## 4. 准备二进制文件（所有节点）
#### 4.1 配置免密登录
为了方便文件的copy我们选择一个中转节点（随便一个节点，可以是集群中的也可以是非集群中的），配置好跟其他所有节点的免密登录
```bash
# 看看是否已经存在rsa公钥
$ cat ~/.ssh/id_rsa.pub

# 如果不存在就创建一个新的
$ ssh-keygen -t rsa

# 把id_rsa.pub文件内容copy到其他机器的授权文件中
$ cat ~/.ssh/id_rsa.pub

# 在其他节点执行下面命令（包括worker节点）
$ echo "<file_content>" >> ~/.ssh/authorized_keys
```

#### 4.2 下载二进制文件
​	链接: https://pan.baidu.com/s/1IJyadpT-aqOBDAwr2gY1rQ 
​    提取码: 5t60

#### 4.3 分发文件并设置好PATH
```bash
# 把文件copy到每个节点上（注意替换自己的文件目录）
# mkdir -p /opt/kubernetes/bin 需要在所有的节点创建 并设置到环境变量中
$ ssh <user>@<node-ip> "mkdir -p /opt/kubernetes/bin"
$ scp master/* <user>@<master-ip>:/opt/kubernetes/bin/
$ scp worker/* <user>@<worker-ip>:/opt/kubernetes/bin/

# 给每个节点设置PATH
$ ssh <user>@<node-ip> "echo 'PATH=/opt/kubernetes/bin:$PATH' >>~/.bashrc"

# 给自己设置path，后面会用到kubectl命令
$ vi ~/.bash_profile
```

## 5. 准备配置文件（中转节点）
上一步我们下载了kubernetes各个组件的二进制文件，这些可执行文件的运行也是需要添加很多参数的，包括有的还会依赖一些配置文件。现在我们就把运行它们需要的参数和配置文件都准备好。
#### 5.1 下载配置文件
我这准备了一个项目，专门为大家按照自己的环境生成配置的。它只是帮助大家尽量的减少了机械化的重复工作。它并不会帮你设置系统环境，不会给你安装软件。总之就是会减少你的部署工作量，但不会耽误你对整个系统的认识和把控。
```bash
$ cd ~
$ git clone https://git.imooc.com/coding-335/kubernetes-ha-binary.git

# 看看git内容
$ ls -l kubernetes-ha-binary
addons/
configs/
pki/
services/
init.sh
global-configs.properties
```
#### 5.2 文件说明
- **addons**
> kubernetes的插件目录，包括calico、coredns、dashboard等。

- **configs**
> 这个目录比较 - 凌乱，包含了部署集群过程中用到的杂七杂八的配置文件、脚本文件等。

- **pki**
> 各个组件的认证授权相关证书配置。

- **services**
> 所有的kubernetes服务(service)配置文件。

- **global-configs.properties**
> 全局配置，包含各种易变的配置内容。

- **init.sh**
> 初始化脚本，配置好global-config之后，会自动生成所有配置文件。

#### 5.3 生成配置
这里会根据大家各自的环境生成kubernetes部署过程需要的配置文件。
在每个节点上都生成一遍，把所有配置都生成好，后面会根据节点类型去使用相关的配置。

```bash
# cd到之前下载的git代码目录
$ cd kubernetes-ha-binary

# 编辑属性配置（根据文件注释中的说明填写好每个key-value）
$ vi global-config.properties

# 生成配置文件，确保执行过程没有异常信息
$ ./init.sh

# 查看生成的配置文件，确保脚本执行成功
$ find target/ -type f
```
> **执行init.sh常见问题：**
>
> 1. Syntax error: "(" unexpected
> - bash版本过低，运行：bash -version查看版本，如果小于4需要升级
> - 不要使用 sh init.sh的方式运行（sh和bash可能不一样哦）
> 2. global-config.properties文件填写错误，需要重新生成
> 再执行一次./init.sh即可，不需要手动删除target

#### 5.4 配置文件说明

```bash
#3个master节点的ip
MASTER_0_IP=172.18.41.18
MASTER_1_IP=172.18.41.19
MASTER_2_IP=172.18.41.20
#3个master节点的hostname
MASTER_0_HOSTNAME=m7-a2-15-41.18-jiagou.cn
MASTER_1_HOSTNAME=m7-a2-15-41.19-jiagou.cn
MASTER_2_HOSTNAME=m7-a2-15-41.20-jiagou.cn
#api-server的高可用虚拟ip, 这是一个真实的IP地址
MASTER_VIP=172.18.41.14
#keepalived用到的网卡接口名，一般是eth0 使用 ip a 命令查看
VIP_IF=eth0
#worker节点的ip列表
WORKER_IPS=172.18.64.41,172.18.64.42
#kubernetes服务ip网段
SERVICE_CIDR=10.254.0.0/16
#kubernetes的默认服务ip，一般是cidr的第一个
KUBERNETES_SVC_IP=10.254.0.1
#dns服务的ip地址，一般是cidr的第二个
CLUSTER_DNS=10.254.0.2
#pod网段
POD_CIDR=172.22.0.0/16
#NodePort的取值范围
NODE_PORT_RANGE=8400-8900

```