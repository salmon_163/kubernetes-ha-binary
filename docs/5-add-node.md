# 向现有集群中添加新的node节点

## 1. 完成第一步中的环境准备条件 
[链接1-prepare.md](https://gitee.com/salmon_163/kubernetes-ha-binary/blob/master/docs/1-prepare.md)
## 2. 第一步中添加 worker节点的ip列表,重新执行 ./init.sh, 重新生成新节点的配置
## 3. 生成token进行替换, 在能运行kubeclt的的机器上
```bash
# 查看生成的token， 如果kubeadm token list 能查询到令牌，不生成直接使用也成
$ kubeadm token list
# 生成新的token， 记录下这个令牌，后面会用到
$ kubeadm token create
  ebo8kz.ehiw8snjrzbge8os
```

## 4. 安装kubelet
```bash
# 拷贝需要的文件, 在新添加的node节点上执行如下命令
# 在node节点上创建目录
$ mkdir -p /etc/kubernetes/pki
# 创建 /opt/kubernetes/bin/ 目录 并将此目录添加到环境变量中 PATH
$ mkdir -p /opt/kubernetes/bin/
# 从已经在集群环境中的woker节点，拷贝ca.pem到新添加的node节点上
# 此命令在新的node节点上运行
$ scp <user>@<node-ip>:/etc/kubernetes/pki/ca.pem /etc/kubernetes/pki/
$ scp <user>@<node-ip>:/etc/kubernetes/kubelet-bootstrap.kubeconfig /etc/kubernetes/
$ scp <user>@<node-ip>:/opt/kubernetes/bin/* /opt/kubernetes/bin/
# 编辑kubelet-bootstrap.kubeconfig 将 文件中的token替换成新生成的token ebo8kz.ehiw8snjrzbge8os
$ sed -i -r '$s/token.+/token: ebo8kz.ehiw8snjrzbge8os/' /etc/kubernetes/kubelet-bootstrap.kubeconfig
# 转到中转节点，就是重新生成配置的那台机器上，拷贝的新节点上
$ scp target/worker-<node-ip>/kubelet.config.json <user>@<node-ip>:/etc/kubernetes/
$ scp target/worker-<node-ip>/kubelet.service <user>@<node-ip>:/etc/systemd/system/

# 在新添加的节点上创建工作目录
$ mkdir -p /var/lib/kubelet
# 启动服务 在节点上
$ systemctl daemon-reload && systemctl enable kubelet && systemctl restart kubelet

# 在 master 上Approve bootstrap请求， 在运行kubectl命令的那台节点上
$ kubectl get csr
# <name> 的值是 运行 kubectl get csr 返回的NAME
$ kubectl certificate approve <name> 

# 查看服务状态 在每个woker节点上
$ systemctl status kubelet

# 查看日志 在每个woker节点上
$ journalctl -f -u kubelet
```
## 5. 安装kube-proxy
```bash
# 从已经在集群环境中的woker节点，拷贝kube-proxy.kubeconfig到新添加的node节点上
# 此命令在新的node节点上运行
$ scp <user>@<node-ip>:/etc/kubernetes/kube-proxy.kubeconfig /etc/kubernetes/
$ scp <user>@<node-ip>:/etc/systemd/system/kube-proxy.service /etc/systemd/system/

# 转到中转节点，就是重新生成配置的那台机器上，拷贝的新节点上
$ scp target/worker-<node-ip>/kube-proxy.config.yaml <user>@<node-ip>:/etc/kubernetes/

# 运行如下命令启动服务
# 创建依赖目录 每个woker节点
$ mkdir -p /var/lib/kube-proxy && mkdir -p /var/log/kubernetes

# 启动服务
$ systemctl daemon-reload && systemctl enable kube-proxy && systemctl restart kube-proxy

# 查看状态
$ systemctl status kube-proxy

# 查看日志
$ journalctl -f -u kube-proxy
```