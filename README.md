# kubernetes-ha-binary - 1.14

## 项目介绍
项目致力于让有意向使用原生kubernetes集群的企业或个人，可以方便的、系统的使用**二进制**的方式手工搭建kubernetes高可用集群。并且让相关的人员可以更好的理解kubernetes集群的运作机制。

## 软件版本
- os centos7.2（ubuntu也适用，需要替换部分命令）
- kubernetes 1.14
- etcd 3.2.18
- docker 18.09.2
- calico 3.1.3

## 安装教程
#### [一、实践环境准备][1]
#### [二、高可用集群部署][2]
#### [三、集群可用性测试][3]
#### [四、部署dashboard][4]
#### [五、在集群中添加新节点][5]

[1]:https://gitee.com/salmon_163/kubernetes-ha-binary/blob/master/docs/1-prepare.md
[2]:https://gitee.com/salmon_163/kubernetes-ha-binary/blob/master/docs/2-ha-deploy.md
[3]:https://gitee.com/salmon_163/kubernetes-ha-binary/blob/master/docs/3-test.md
[4]:https://gitee.com/salmon_163/kubernetes-ha-binary/blob/master/docs/4-dashboard.md
[5]:https://gitee.com/salmon_163/kubernetes-ha-binary/blob/master/docs/5-add-node.md