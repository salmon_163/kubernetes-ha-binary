#!/bin/bash

docker pull registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:kubernetes-dashboard-amd64-v1.10.1
docker pull registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-node-v3.1.3
docker pull registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-typha-v0.7.4
docker pull registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-cni-v3.1.3
docker pull registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:coredns-1.1.3
docker pull registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:pause-amd64-3.1

docker tag registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:kubernetes-dashboard-amd64-v1.10.1 k8s.gcr.io/kubernetes-dashboard-amd64:v1.10.1
docker tag registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-node-v3.1.3 quay.io/calico/node:v3.1.3
docker tag registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-typha-v0.7.4 quay.io/calico/typha:v0.7.4
docker tag registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-cni-v3.1.3 quay.io/calico/cni:v3.1.3
docker tag registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:coredns-1.1.3 k8s.gcr.io/coredns:1.1.3
docker tag registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:pause-amd64-3.1 k8s.gcr.io/pause-amd64:3.1

docker rmi registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:kubernetes-dashboard-amd64-v1.10.1
docker rmi registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-node-v3.1.3
docker rmi registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-typha-v0.7.4
docker rmi registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:calico-cni-v3.1.3
docker rmi registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:coredns-1.1.3
docker rmi registry.cn-qingdao.aliyuncs.com/salmon/kubernetes:pause-amd64-3.1
